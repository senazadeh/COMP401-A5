package a5;

public class GreenPlate implements Plate {
	private double price = 2.0;
	private Sushi contents = null;
	
	public GreenPlate(Sushi contents) throws PlatePriceException {
		this.contents = contents;
	}
	
	
	public Sushi getContents() {
		return this.contents;
	}

	public Sushi removeContents() {
		Sushi priorContents = this.contents;
		if (this.contents != null) {
			this.contents = null;
		}
		return priorContents;
	}

	public void setContents(Sushi s) throws PlatePriceException {
		if (s == null) {
			throw new IllegalArgumentException("s is null");
		} else if (this.price <= s.getCost()) {
			throw new PlatePriceException("no profit");
		} else {
		this.contents = s;
		}
	}

	public boolean hasContents() {
		if (this.contents == null) {
			return false;
		} else {
			return true;
		}
	}

	public double getPrice() {
		return this.price;
	}

	public Color getColor() {
		return Color.GREEN;
	}

	public double getProfit() {
		if (this.contents == null) {
			return 0.0;
		} else {
			double profit = this.price - contents.getCost() - 0.005;
			return Math.round(profit * 100.00) / 100.00;
		}
	}

}
