package a4;

public class Sashimi implements Sushi{
	private SashimiType type = null;
	private IngredientPortion seafood = null;
	private static double ouncesOfSashimi = 0.75;
	
	public enum SashimiType {TUNA, YELLOWTAIL, EEL, CRAB, SHRIMP};
	
	public Sashimi(SashimiType type) {
		this.type = type;
		
	if (this.type == SashimiType.TUNA) {
		this.seafood = new TunaPortion(ouncesOfSashimi);
	}
	if (this.type == SashimiType.YELLOWTAIL) {
		this.seafood = new YellowtailPortion(ouncesOfSashimi);
	}
	if (this.type == SashimiType.EEL) {
		this.seafood = new EelPortion(ouncesOfSashimi);
	}
	if (this.type == SashimiType.CRAB) {
		this.seafood = new CrabPortion(ouncesOfSashimi);
	}
	if (this.type == SashimiType.SHRIMP) {
		this.seafood = new ShrimpPortion(ouncesOfSashimi);
	}
}
	
	public String getName() {
		return this.seafood.getName() + " sashimi";
	}

	public IngredientPortion[] getIngredients() {
		IngredientPortion[] sashimiPortions = new IngredientPortion[] {this.seafood};
		return sashimiPortions;
	}

	public int getCalories() {
		return (int) (this.seafood.getCalories() + 0.5);
	}

	public double getCost() {
		return this.seafood.getCost();
	}
	
	public boolean getHasRice() {
		return false;
	}

	public boolean getHasShellfish() {
		return this.seafood.getIsShellfish();
	}

	public boolean getIsVegetarian() {
		return false;
	}
	
}
