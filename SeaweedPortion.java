package a4;

public class SeaweedPortion implements IngredientPortion{
	private static Ingredient Seaweed = new Seaweed(); 
	private double amount = 0;
	
	public SeaweedPortion (double amount) {
		if (amount < 0) {
			throw new RuntimeException("Amount is less than 0"); }
	
	this.amount = amount; 
	}

	public Ingredient getIngredient() {
		return Seaweed;
	}

	public String getName() {
		return Seaweed.getName();
	}

	public double getAmount() {
		return amount;
	}

	public double getCalories() {
		return Seaweed.getCaloriesPerOunce() * amount;
	}

	public double getCost() {
		return Seaweed.getPricePerOunce() * amount;
	}

	public boolean getIsVegetarian() {
		return Seaweed.getIsVegetarian();
	}

	public boolean getIsRice() {
		return Seaweed.getIsRice();
	}

	public boolean getIsShellfish() {
		return Seaweed.getIsShellfish();
	}

	public IngredientPortion combine(IngredientPortion other) {
		if (other == null) {
			return this; 
		} else if (!this.getName().equals(other.getName())) {
			throw new RuntimeException("Not the same ingregient"); 
		} else {
			SeaweedPortion combinedSeaweed = new SeaweedPortion(this.getAmount() + other.getAmount());
			return combinedSeaweed;
		}
	}	
}
