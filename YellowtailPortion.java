package a4;

public class YellowtailPortion implements IngredientPortion{
	private static Ingredient Yellowtail = new Yellowtail(); 
	private double amount = 0;
	
	public YellowtailPortion (double amount) {
		if (amount < 0) {
			throw new RuntimeException("Amount is less than 0"); }
	
	this.amount = amount; 
	}

	public Ingredient getIngredient() {
		return Yellowtail;
	}

	public String getName() {
		return Yellowtail.getName();
	}

	public double getAmount() {
		return amount;
	}

	public double getCalories() {
		return Yellowtail.getCaloriesPerOunce() * amount;
	}

	public double getCost() {
		return Yellowtail.getPricePerOunce() * amount;
	}

	public boolean getIsVegetarian() {
		return Yellowtail.getIsVegetarian();
	}

	public boolean getIsRice() {
		return Yellowtail.getIsRice();
	}

	public boolean getIsShellfish() {
		return Yellowtail.getIsShellfish();
	}

	public IngredientPortion combine(IngredientPortion other) {
		if (other == null) {
			return this; 
		} else if (!this.getName().equals(other.getName())) {
			throw new RuntimeException("Not the same ingregient"); 
		} else {
			YellowtailPortion combinedYellowtail = new YellowtailPortion(this.getAmount() + other.getAmount());
			return combinedYellowtail;
		}
	}	
}
