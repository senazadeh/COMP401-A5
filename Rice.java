package a4;

public class Rice extends IngredientsParent implements Ingredient{
	private static String name = "rice";
	private static double pricePerOz =	0.13;
	private static int caloriesPerOz =	34;
	private static boolean Vegetarian = true;
	private static boolean Rice = true;
	private static boolean Shellfish =	false;
	
	public Rice() { 
		super(name, pricePerOz, caloriesPerOz, Vegetarian, Rice, Shellfish);
	}
	
	public String getName() {
		return name;
	}
	
	public double getCaloriesPerDollar() {
		return caloriesPerOz / pricePerOz;
	}
	
	public int getCaloriesPerOunce() {
		return caloriesPerOz;
	}
	
	public double getPricePerOunce() {
		return pricePerOz;
	}
	
	public boolean equals(Ingredient other) {
		return (this.getName().equals(other.getName()) &&
				this.getCaloriesPerOunce() == other.getCaloriesPerOunce() &&
				this.getPricePerOunce() - other.getPricePerOunce() < 0.01 && 
				this.getPricePerOunce() - other.getPricePerOunce() > -0.01 &&
				this.getIsVegetarian() == other.getIsVegetarian() &&
				this.getIsRice() == other.getIsRice() &&
				this.getIsShellfish() == other.getIsShellfish()); 
	}
	
	public boolean getIsVegetarian() {
		return Vegetarian;
	}
	
	public boolean getIsRice() {
		return Rice;
	}
	
	public boolean getIsShellfish() {
		return Shellfish;
	}
}

